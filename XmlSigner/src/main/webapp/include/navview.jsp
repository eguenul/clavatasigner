<nav class="navbar navbar-default" role="navigation">
  <div class="navbar-header">
    <!-- Se eliminó el contenido vacío -->
  </div>

  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-cog"></span> MANTENCION<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
         <li><a href="usuario">USUARIOS</a></li>
            <li><a href="adminCert">CARGA CERTIFICADO DIGITAL</a></li>
          <li><a href="setFirmaPass">CLAVE FIRMA ELECTRONICA</a></li>
          <li><a href="correlativo">CORRELATIVOS</a></li>
          <li><a href="setpass">CAMBIO PASSWORD ADMINISTRADOR</a></li>
        </ul>
      </li>
       <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-tasks"></span>PUNTO DE EVENTA<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="boletaservlet">EMISION BOLETA DE VENTA</a></li>
           </ul>
      </li>
      
      
      
      
      
      
   
    </ul>

    <ul class="nav navbar-nav navbar-right">
      
      <li><a href="#">
        <span class="glyphicon glyphicon-user"></span>
        <% out.print(request.getSession().getAttribute("login")); %>
      </a></li>

      <li>
        <a href="logout.jsp">
          <span class="glyphicon glyphicon-log-out"></span> SALIR
        </a>
      </li>
    </ul>
  </div>
</nav>
