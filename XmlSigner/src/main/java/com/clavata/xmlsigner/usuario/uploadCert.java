package com.clavata.xmlsigner.usuario;

import com.clavata.xmlsigner.include.Funciones;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

@WebServlet(urlPatterns = "/uploadCert", name = "uploadCert")
@MultipartConfig(
    fileSizeThreshold = 1024 * 1024 * 2,  // 2MB
    maxFileSize = 1024 * 1024 * 10,       // 10MB
    maxRequestSize = 1024 * 1024 * 50     // 50MB
)
public class uploadCert extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        byte[] certificateBytes = null;
        String login = (String) request.getSession().getAttribute("login");

        try {
            // Obtenemos la parte del archivo de la solicitud
            Part filePart = request.getPart("file");

            // Leemos los bytes del archivo subido
            certificateBytes = readBytesFromPart(filePart);

            // Guardar el certificado utilizando la función `addCertificado`
            Funciones objFunciones = new Funciones();
            objFunciones.addCertificado(login, certificateBytes);

            // Archivo subido exitosamente
            System.out.println("CERTIFICADO CORRECTAMENTE CARGADO");
            response.sendRedirect("messageview/cafok.html");
        } catch (ServletException | IOException | ClassNotFoundException | SQLException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(uploadCert.class.getName()).log(Level.SEVERE, "File Upload Failed due to: " + ex.getMessage(), ex);
            request.setAttribute("message", "File Upload Failed due to " + ex);
            request.getRequestDispatcher("messageview/error.html").forward(request, response);
        }
    }

    private byte[] readBytesFromPart(Part part) throws IOException {
        InputStream inputStream = part.getInputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, bytesRead);
        }

        return byteArrayOutputStream.toByteArray();
    }
}