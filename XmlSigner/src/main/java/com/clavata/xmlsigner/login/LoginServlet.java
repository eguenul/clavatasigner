 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clavata.xmlsigner.login;

import com.clavata.xmlsigner.include.Funciones;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

@WebServlet(urlPatterns = "/login", name = "LoginServlet")
public class LoginServlet  extends HttpServlet {
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
      try {
          String login = request.getParameter("login");
          String clave = request.getParameter("clave");
          LoginModel objLoginModel = new LoginModel();
          if( objLoginModel.authLogin(login, clave)==true){
              
              
              request.getSession().setAttribute("login",login);
              request.getSession().setAttribute("loginauth", "yes");
             
              
               response.sendRedirect("index.jsp");
              
              if("admin".equals(login)==false){
                  Funciones objFunciones = new Funciones();
   try{
                  objFunciones.loadCert(login);
               
             
             
   }catch(NullPointerException ex2)      
              {
                response.sendRedirect("messageview/errorcertificado.html");
                
              }   catch (ParserConfigurationException | SAXException | ClassNotFoundException ex) {
                      Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
                  }
               
              }
          }else{
              response.sendRedirect("messageview/errorlogin.html");
         } 
          
          
          
      } catch (SQLException ex) {
          Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
      }
         
      }
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
  
   if(request.getSession().getAttribute("loginauth") != "yes"){
      getServletConfig().getServletContext().getRequestDispatcher("/loginview/login.jsp").forward(request,response);   
   }else{
       
       response.sendRedirect("index.jsp");
   }
      
  }
 
  
 
  
  
  
}
