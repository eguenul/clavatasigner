/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.clavata.xmlsigner.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author esteban
 */
public class TokenSignerService {
    public String signToken(String valorsemilla, String path_certificado, String clave) throws ParserConfigurationException, TransformerConfigurationException, TransformerException, KeyException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyStoreException, FileNotFoundException, IOException, CertificateException, UnrecoverableEntryException, MarshalException, XMLSignatureException{
    
       
        
        
         /* CREO LOS ELEMENTOS DE FIRMA */     
        // Create a DOM XMLSignatureFactory that will be used to
        // generate the enveloped signature.
            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case,
        // you are signing the whole document, so a URI of "" signifies
        // that, and also specify the SHA1 digest algorithm and
        // the ENVELOPED Transform.
            Reference ref = fac.newReference
             ("", fac.newDigestMethod(DigestMethod.SHA1, null),
              Collections.singletonList
               (fac.newTransform
                (Transform.ENVELOPED, (TransformParameterSpec) null)),
                 null, null);

            // Create the SignedInfo.
            SignedInfo si = fac.newSignedInfo
             (fac.newCanonicalizationMethod
              (CanonicalizationMethod.INCLUSIVE,
               (C14NMethodParameterSpec) null),
                fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
                 Collections.singletonList(ref));


        /* instancio el certificado digital */
        KeyStore p12 = KeyStore.getInstance("pkcs12");
        
        
        System.out.print(path_certificado+".pfx");
        p12.load(new FileInputStream(path_certificado+".pfx"), clave.toCharArray());
        Enumeration e = p12.aliases();
        String alias = (String) e.nextElement();
        System.out.println("Alias certifikata:" + alias);
        KeyStore.PrivateKeyEntry keyEntry = (KeyStore.PrivateKeyEntry) p12.getEntry(alias, new KeyStore.PasswordProtection(clave.toCharArray()));
       
        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();
    
        // Create the KeyInfo containing the X509Data.
        KeyInfoFactory kif = fac.getKeyInfoFactory();
        List x509Content = new ArrayList();
        
        x509Content.add(cert);
        X509Data xd = kif.newX509Data(x509Content);
    
        KeyValue keyValue = kif.newKeyValue(cert.getPublicKey());
        ArrayList itemcert = new ArrayList();
        itemcert.add(keyValue);
        itemcert.add(xd);
        
        /*
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));
        */
         KeyInfo ki = kif.newKeyInfo(itemcert);
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /* creo el xml de solicitud de token */
        
        
        valorsemilla = valorsemilla.replaceFirst ("^0*", "");
        Long cadenaResultadoInt = Long.parseLong(valorsemilla);
           
        String valuesemilla = Long.toString(cadenaResultadoInt);
          
        
       
          
        
       DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
       DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
       Document doc = docBuilder.newDocument();
         
       Element gettoken = doc.createElement("getToken");
       Element item = doc.createElement("item");
         
        
       Node semilla = doc.createElement("Semilla");
       semilla.setTextContent(valuesemilla);
       item.appendChild(semilla);
         
       gettoken.appendChild(item);
       doc.appendChild(gettoken);
       
       
       
       /* ahora computo la firma  */
       DOMSignContext dsc = new DOMSignContext
        (keyEntry.getPrivateKey(), doc.getDocumentElement());

// Create the XMLSignature, but don't sign it yet.
        XMLSignature signature = fac.newXMLSignature(si, ki);

// Marshal, generate, and sign the enveloped signature.
   signature.sign(dsc);

       
       
       
       
       
       TransformerFactory transformerFactory = TransformerFactory.newInstance();
       Transformer transformer = transformerFactory.newTransformer();
       DOMSource source = new DOMSource(doc);
         
       
       
       
       
       
       
       StringWriter writer = new StringWriter();
         
	 StreamResult result = new StreamResult(writer);
	
         transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
          transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
          transformer.setOutputProperty(OutputKeys.INDENT, "yes");
          transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
          transformer.transform(source, result);
	  System.out.println("Done");
           
          
          
      
         return writer.toString();
           
    }
    
   
    
    
    
}
