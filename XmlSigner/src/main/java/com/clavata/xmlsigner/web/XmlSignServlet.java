/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.clavata.xmlsigner.web;
import java.io.IOException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
import com.clavata.xmlsigner.service.XmlSignerService;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;

@WebServlet("/signxml")
public class XmlSignServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    private final XmlSignerService xmlSignerService;
    
    public XmlSignServlet() {
        super();
        xmlSignerService = new XmlSignerService(); // Instancia del servicio de firma XML
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Obtener los parámetros del request
        String xmlString = request.getParameter("xml");
        String pathCertificate = request.getParameter("pathCertificate");
        String password = request.getParameter("password");
        String idXml = request.getParameter("idXml");
        String nodeXml = request.getParameter("nodeXml");
        
        try {
            String signedXml = xmlSignerService.signXml(xmlString, pathCertificate, password, idXml, nodeXml);
            
            response.setContentType("application/xml");
            response.setCharacterEncoding("ISO-88859-1");
            response.getWriter().write(signedXml);
        } catch (ParserConfigurationException | TransformerException | SAXException |
                 IOException e) {
            // Manejo de excepciones y devolución de error
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Error al firmar el XML: " + e.getMessage());
        } catch (MarshalException | XMLSignatureException | KeyException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | KeyStoreException | CertificateException | UnrecoverableEntryException ex) {
            Logger.getLogger(XmlSignServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
