package com.clavata.xmlsigner.web;
import com.clavata.xmlsigner.service.TokenSignerService;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author esteban
 */
@WebServlet(name = "TokenSignServlet", urlPatterns = {"/token-sign"})
public class TokenSignServlet extends HttpServlet {

    private final TokenSignerService tokenSignerService = new TokenSignerService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Configurar el tipo de contenido de la respuesta
        response.setContentType("application/xml;charset=UTF-8");

        // Leer el valor semilla y la ruta del certificado del request
        String valorSemilla = request.getParameter("semilla");
        String pathCertificado = request.getParameter("certificado");
        String clave = request.getParameter("clave");

        try {
            // Firmar el token XML con los parámetros recibidos
            String signedToken = tokenSignerService.signToken(valorSemilla, pathCertificado, clave);

            // Escribir la respuesta firmada en el cuerpo de la respuesta HTTP
            try (PrintWriter out = response.getWriter()) {
                out.println(signedToken);
            }
        } catch (IOException | InvalidAlgorithmParameterException | KeyException | KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException | CertificateException | MarshalException | XMLSignatureException | ParserConfigurationException | TransformerException e) {
            // Manejar cualquier otra excepción y enviar una respuesta de error
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            try (PrintWriter out = response.getWriter()) {
                out.println("Error interno al procesar la solicitud: " + e.getMessage());
            }
        }
    }
}