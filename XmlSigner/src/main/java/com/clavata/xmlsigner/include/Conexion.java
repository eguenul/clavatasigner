/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.clavata.xmlsigner.include;
import java.io.IOException;
import java.sql.*;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
public class Conexion {
   private  Connection cnx = null;
   public void Conectar() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException {
      if (this.cnx == null) {
         try {
            System.out.print("conectando");
          ClavataConfig objconfig = new ClavataConfig();
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.cnx = DriverManager.getConnection("jdbc:mysql://"+objconfig.getServeraddress()+":3306/"+objconfig.getDatabasename()+"?allowPublicKeyRetrieval=true&useSSL=false", objconfig.getUsername(),objconfig.getUserpass());
            
            Statement stmt2 = cnx.createStatement();
             stmt2.execute("SET CHARACTER SET utf8");
            
         } catch (SQLException ex) {
            throw new SQLException(ex);
         } catch (ClassNotFoundException ex) {
            throw new ClassCastException(ex.getMessage());
         }
      }
     
   }
   public  void cerrar() throws SQLException {
     
         this.cnx.close();
      
   }
   
   public  Connection getConexion() throws SQLException {
     
        return this.cnx;
      
   }

    public Connection obtener() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
   
   
}