/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clavata.xmlsigner.include;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class ClavataConfig {
    
  private String serveraddress;
  private String username;
  private String userpass;
  private String databasename;

    
    
    public ClavataConfig() throws ParserConfigurationException, SAXException, IOException{
       
        
        Properties prop = new Properties();
        try (InputStream in = getClass().getResourceAsStream("/clavatasigner.properties")) {
            prop.load(in);
       
          this.serveraddress = prop.getProperty("server-address");
          this.username = prop.getProperty("user-name");
          this.userpass = prop.getProperty("user-pass");
          this.databasename = prop.getProperty("database-name");
          
        }
        
      

}

    public String getServeraddress() {
        return serveraddress;
    }

    public void setServeraddress(String serveraddress) {
        this.serveraddress = serveraddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }

    public String getDatabasename() {
        return databasename;
    }

    public void setDatabasename(String databasename) {
        this.databasename = databasename;
    }
}
